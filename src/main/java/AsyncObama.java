import decorator.NewsDecoratorInterface;
import decorator.NewsDecoratorService;
import generator.NewsGeneratorInterface;
import generator.NewsGeneratorService;
import processor.NewProcessorService;
import processor.NewsProcessorInterface;
import processor.Rule;

import java.util.List;

public class AsyncObama implements NewsGeneratorService.Listener, NewProcessorService.Listener, NewsDecoratorService.Listener {

    private static final String PATH_NEWS_INPUT_FILE = "./src/main/resources/news_strings.txt";
    private static final String PATH_NEWS_OUTPUT_FILE = "./src/main/resources/result.txt";

    private final NewsGeneratorInterface generator;
    private final NewsProcessorInterface processor;
    private final NewsDecoratorInterface decorator;

    public AsyncObama() {
        generator = new NewsGeneratorService(PATH_NEWS_INPUT_FILE);
        processor = new NewProcessorService();
        decorator = new NewsDecoratorService(PATH_NEWS_OUTPUT_FILE);
    }

    public void run() {
        generator.asyncGetNextNews(this);
    }

    @Override
    public void newsRead(List<String> news) {
        processor.asyncProcessNews(news, this);
    }

    @Override
    public void newsProcessed(List<Rule> rules, List<String> news) {
        decorator.asyncDecorateNews(rules, news, this);
    }

    @Override
    public void newsDecorated(List<String> result) {
        //Do nothing due to news write into the file "result.txt"
    }
}
