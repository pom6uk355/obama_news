package generator;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class NewsGeneratorService implements NewsGeneratorInterface {

    private BufferedReader reader;
    private final String pathIn;

    public NewsGeneratorService(String pathIn) {
        this.pathIn = pathIn;
    }

    @Override
    public List<String> getNextNews() {
        BufferedReader bufferedReader = getReader();
        if (bufferedReader == null) {
            return Collections.emptyList();
        }
        List<String> lines = new ArrayList<>();
        try {
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                lines.add(line);
            }
            bufferedReader.close();
            return lines;
        } catch (IOException e) {
            System.err.println("Can't read line.");
            e.printStackTrace();
            return Collections.emptyList();
        }
    }

    @Override
    public void asyncGetNextNews(Listener listener) {
        new Thread(() -> listener.newsRead(getNextNews())).start();
    }

    public interface Listener {
        void newsRead(List<String> news);
    }

    private BufferedReader getReader() {
        if (reader == null) {
            try {
                reader = new BufferedReader(new FileReader(pathIn));
            } catch (FileNotFoundException e) {
                System.err.println("Can't locate reader.");
                e.printStackTrace();
            }
        }
        return reader;
    }
}
