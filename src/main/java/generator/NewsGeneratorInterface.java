package generator;

import java.util.List;

public interface NewsGeneratorInterface {

    List<String> getNextNews();

    void asyncGetNextNews(NewsGeneratorService.Listener listener);
}
