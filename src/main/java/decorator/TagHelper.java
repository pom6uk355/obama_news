package decorator;

public class TagHelper {

    public static final String ENTITY_TAG = "ENTITY_TAG";
    public static final String LINK_TAG = "LINK_TAG";
    public static final String TWITTER_TAG = "TWITTER_TAG";

    public static void parseEntityTag(StringBuilder builder, String partNewsWord) {
        builder.append("<strong>").append(partNewsWord).append("</strong>");
    }

    public static void parseLinkTag(StringBuilder builder, String partNewsLink) {
        builder.append("<a href=\"").append(partNewsLink).append("\">").append(partNewsLink).append("</a>");
    }

    public static void parseTwitterTag(StringBuilder builder, String partNewsTwitter) {
        builder.append("<a href=\"http://twitter.com/").append(partNewsTwitter).append("\">").append(partNewsTwitter).append("/a>");
    }
}
