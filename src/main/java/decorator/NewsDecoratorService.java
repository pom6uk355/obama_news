package decorator;

import processor.Rule;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class NewsDecoratorService implements NewsDecoratorInterface {

    private BufferedWriter writer;
    private final String pathOut;

    public NewsDecoratorService(String pathOut) {
        this.pathOut = pathOut;
    }

    @Override
    public List<String> writeDecoratedNews(List<Rule> rules, List<String> newsList) {
        List<String> resList = getDecoratedNews(rules, newsList);
        BufferedWriter writer = getWriter();
        if (writer == null) {
            return Collections.emptyList();
        }
        try {
            for (String s : resList) {
                writer.write(s);
                writer.write('\n');
            }
            writer.close();
        } catch (IOException e) {
            System.err.println("Can't write string.");
            e.printStackTrace();
        }
        return resList;
    }

    @Override
    public void asyncDecorateNews(List<Rule> rules, List<String> news, Listener listener) {
        new Thread(() -> listener.newsDecorated(writeDecoratedNews(rules, news))).start();
    }

    public interface Listener {
        void newsDecorated(List<String> result);
    }

    private List<String> getDecoratedNews(List<Rule> rules, List<String> newsList) {
        List<String> resList = new ArrayList<>();

        newsList.stream().filter(n -> !n.isEmpty()).forEach(newsItem -> {
            // Builder for each news item
            StringBuilder resNewsItemBuilder = new StringBuilder();

            if (!rules.isEmpty()) {
                final int[] nextRuleIndex = {1};
                final int size = rules.size();
                rules.forEach(rule -> {
                    int start = rule.startPos;
                    int end = rule.endPos;
                    String ruleName = rule.name;
                    if (TagHelper.ENTITY_TAG.equalsIgnoreCase(ruleName)) {
                        TagHelper.parseEntityTag(resNewsItemBuilder, newsItem.substring(start, end));
                    } else if (TagHelper.LINK_TAG.equalsIgnoreCase(ruleName)) {
                        TagHelper.parseLinkTag(resNewsItemBuilder, newsItem.substring(start, end));
                    } else if (TagHelper.TWITTER_TAG.equalsIgnoreCase(ruleName)) {
                        TagHelper.parseTwitterTag(resNewsItemBuilder, newsItem.substring(start, end));
                    }
                    if (nextRuleIndex[0] < size) {
                        //For non-tagged words
                        resNewsItemBuilder.append(newsItem.substring(end, getNextRuleStartPos(rules, nextRuleIndex[0])));
                    }
                    nextRuleIndex[0]++;
                });
                resList.add(resNewsItemBuilder.toString());
            }
        });
        return resList;
    }

    private int getNextRuleStartPos(List<Rule> rules, int i) {
        return rules.get(i).startPos;
    }

    private BufferedWriter getWriter() {
        if (writer == null) {
            try {
                writer = new BufferedWriter(new FileWriter(pathOut));
            } catch (IOException e) {
                System.err.println("Can't locate writer.");
                e.printStackTrace();
            }
        }
        return writer;
    }
}
