package decorator;

import processor.Rule;

import java.util.List;

public interface NewsDecoratorInterface {
    List<String> writeDecoratedNews(List<Rule> rules, List<String> newsList);

    void asyncDecorateNews(List<Rule> rules, List<String> news, NewsDecoratorService.Listener listener);
}
