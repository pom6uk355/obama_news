package processor;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class NewProcessorService implements NewsProcessorInterface {

    @Override
    public List<Rule> processNews(List<String> news) {
        List<Rule> rules;
        final boolean[] areObamaNewsExist = {false};

        news.forEach(newsItem -> areObamaNewsExist[0] = newsItem.toLowerCase().contains("obama"));

        if (areObamaNewsExist[0]) {
            rules = new ArrayList<>();
            rules.add(new Rule("ENTITY_TAG", 14, 22));
            rules.add(new Rule("ENTITY_TAG", 0, 5));
            rules.add(new Rule("LINK_TAG", 37, 54));
            rules.add(new Rule("TWITTER_TAG", 55, 67));

            // Sorting by start position
            rules.sort(RuleComparator);
        } else {
            rules = Collections.emptyList();
        }
        return rules;
    }

    @Override
    public void asyncProcessNews(List<String> news, Listener listener) {
        new Thread(() -> listener.newsProcessed(processNews(news), news)).start();
    }

    public interface Listener {
        void newsProcessed(List<Rule> rules, List<String> news);
    }

    private Comparator<Rule> RuleComparator = (rule1, rule2) -> {
        Integer int1 = rule1.startPos;
        Integer int2 = rule2.startPos;
        return int1.compareTo(int2);
    };
}
