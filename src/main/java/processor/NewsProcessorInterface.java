package processor;

import java.util.List;

public interface NewsProcessorInterface {
    List<Rule> processNews(List<String> news);

    void asyncProcessNews(List<String> news, NewProcessorService.Listener listener);
}
