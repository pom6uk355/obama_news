package processor;

public class Rule {
    public final String name;
    public final int startPos;
    public final int endPos;

    public Rule(String name, int startPos, int endPos) {
        this.name = name;
        this.startPos = startPos;
        this.endPos = endPos;
    }

    @Override
    public String toString() {
        return "Rule{" +
                "name='" + name + '\'' +
                ", startPos=" + startPos +
                ", endPos=" + endPos +
                '}';
    }
}
